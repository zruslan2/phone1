package com.example.phone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.example.phone.Classes.Models.ClientData;
import com.example.phone.Classes.Models.GetCreditInfoModel;

public class ContractActivity extends AppCompatActivity {

    WebView webView;
    Button close;
    ClientData cd;
    GetCreditInfoModel gci;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contract);

        Intent intent = getIntent();
        cd = (ClientData) intent.getSerializableExtra(ClientData.class.getSimpleName());
        gci = (GetCreditInfoModel) intent.getSerializableExtra(GetCreditInfoModel.class.getSimpleName());

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(getString(R.string.GetDocument)+"?id="+gci.DogNum);

        close = (Button) findViewById(R.id.closeBtn);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContractActivity.this, FirstWin.class);
                intent.putExtra(ClientData.class.getSimpleName(),cd);
                intent.putExtra(GetCreditInfoModel.class.getSimpleName(),gci);
                startActivity(intent);
            }
        });
    }
}
