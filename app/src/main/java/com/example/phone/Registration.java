package com.example.phone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.phone.Classes.Models.DocumentModel;
import com.example.phone.Helpers.BitmapHelper;
import com.example.phone.Helpers.ParseText;
import com.example.phone.Helpers.TextScanner;

import java.io.IOException;

public class Registration extends AppCompatActivity implements SurfaceHolder.Callback,
        Camera.PictureCallback, Camera.PreviewCallback, Camera.AutoFocusCallback{

    //private final int MY_PERMISSIONS_REQUEST_CAMERA = 100;

    Camera camera = null;
    SurfaceHolder surfaceHolder;
    SurfaceView preview;
    Button shotBtn;
    TextView text;
    ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        preview = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceHolder = preview.getHolder();
        surfaceHolder.addCallback(this);
        text = (TextView) findViewById(R.id.text);
        iv = (ImageView) findViewById(R.id.imageView);
        shotBtn = (Button) findViewById(R.id.shotBtn);

        shotBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //shotBtn.setEnabled(false);
                PackageManager pm = getPackageManager();
                if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)){
                    camera.autoFocus(Registration.this);
                }
                else {
                    try {
                        camera.takePicture(null, null, null, Registration.this);
                    } catch (Exception e){
                        e.getMessage();
                    }
                }
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        try {
            if(camera==null)
                camera = Camera.open();
        }
        catch (Exception ex){
            ex.getMessage();
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        if (camera != null)
        {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {
        if (success)
        {
            try {
                camera.takePicture(null, null, null, this);
            } catch (Exception e)
            {
                e.getMessage();
            }
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        try
        {
            Bitmap bm = BitmapFactory.decodeByteArray(data,0,data.length);

            bm = BitmapHelper.RotateBitmap(bm,90);

            String res = extractText(bm);
            //iv.setImageBitmap(bm);
           /* Toast toast = Toast.makeText(this, res,Toast.LENGTH_LONG);
            toast.show();*/
            //shotBtn.setEnabled(true);
            camera.startPreview();
        }
        catch (Exception e)
        {
            e.getMessage();
        }
        //camera.startPreview();
    }

    private String extractText(Bitmap bitmap) throws Exception
    {
        DocumentModel dm = null;
        String res = null;
        TextScanner ts = new TextScanner(this);
        ts.init();
        res = ts.load(bitmap);
        //Boolean b = ParseText.CheckText(res);
        if(res!=null)
        {
            dm = ParseText.GetDocumentModel(res);
        }
        if (dm==null){
            Intent intent = new Intent(Registration.this, Registration.class);
            startActivity(intent);
        }
        else{
            Intent intent = new Intent(Registration.this, RegistrationFirstWin.class);
            startActivity(intent);
        }
        //IIN = ParseText.GetIIN(MRZ);
        //Boolean b = ParseText.CheckIIN("880201400982");
        //Boolean b = ParseText.CheckID("0235591119");
        //text.setText(res);
        return dm.toString();
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try
        {
            camera.setPreviewDisplay(holder);
            camera.setPreviewCallback(this);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        Camera.Parameters parameters=camera.getParameters();
        Camera.Size size = getMaxSize(parameters);
        parameters.setPictureSize(size.width,size.height);
        camera.setParameters(parameters);

        Camera.Size previewSize = camera.getParameters().getPreviewSize();
        float aspect = (float) previewSize.width / previewSize.height;
        int previewSurfaceWidth = preview.getWidth();
        int previewSurfaceHeight = preview.getHeight();
        ViewGroup.LayoutParams lp = preview.getLayoutParams();
        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE)
        {
            camera.setDisplayOrientation(90);
            lp.height = previewSurfaceHeight;
            lp.width = (int) (previewSurfaceHeight / aspect);
        }
        else
        {
            camera.setDisplayOrientation(0);
            lp.width = previewSurfaceWidth;
            lp.height = (int) (previewSurfaceWidth / aspect);
        }

        preview.setLayoutParams(lp);
        camera.startPreview();
    }

    private Camera.Size getMaxSize(Camera.Parameters param){
        Camera.Size result = null;
        Integer height=0;
        Integer width=0;
        for(Camera.Size size : param.getSupportedPictureSizes()){
            Integer Area = height*width;
            Integer newArea = size.height*size.width;
            if(newArea>Area){
                result = size;
                height = size.height;
                width = size.width;
            }
        }
        return result;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
