package com.example.phone;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.phone.Classes.Callback;
import com.example.phone.Classes.MemoryType;
import com.example.phone.Classes.Models.ClientData;
import com.example.phone.Classes.Models.GetCreditInfoModel;
import com.example.phone.Helpers.HttpWeb;
import com.example.phone.Helpers.IsInteger;
import com.example.phone.Helpers.JsonHelper;
import com.example.phone.Helpers.PrefHelper;


public class MainActivity extends AppCompatActivity implements Callback {

    String phoneNumb = null;
    String clientId;
    String phoneNumbW;
    ProgressBar progress;
//    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //clientId = (String) PrefHelper.getValue(this,"clientId", MemoryType.STRING);
        progress = (ProgressBar) findViewById(R.id.progressBar);
/*        tv = (TextView) findViewById(R.id.textView2);
        tv.setText(clientId);*/
        final EditText phone = (EditText) findViewById(R.id.editText);

        phone.setSelection(phone.getText().length());

        phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        phone.addTextChangedListener(new TextWatcher() {
            Boolean isBackspaceClicked;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (after < count) {
                    isBackspaceClicked = true;
                } else {
                    isBackspaceClicked = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(isBackspaceClicked && s.length()==2)
                {
                    phone.setText("+77");
                    phone.setSelection(phone.getText().length());
                }
                if (s.length() == 15) {
                    progress.setVisibility(View.VISIBLE);
                    phoneNumbW = s.toString().substring(2,s.length()).replaceAll(" ","");

                    HttpWeb http = new HttpWeb(getString(R.string.SendPhoneConfirmationCode),
                            "?PhoneNumber="+phoneNumbW,false, MainActivity.this);
                    http.registerCallBack(MainActivity.this);
                    http.execute();

                    phone.setEnabled(false);
                    //phoneNumb = phone.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!IsInteger.IsaInteger(s.toString().substring(s.length()-1)))
                {
                    phone.setText(s.toString().substring(0,s.length()-1));
                    phone.setSelection(phone.getText().length());
                }
                if(s.toString().length()>15){
                    phone.setText(s.toString().substring(0,s.length()-1));
                    phone.setSelection(phone.getText().length());
                }
            }
        });
    }

    @Override
    public void callBackReturn(String json) {
        if (json.equals("\"ok\"")) {
            Intent intent = new Intent(MainActivity.this, GetCode.class);
            //intent.putExtra("clientId", clientId);
            intent.putExtra("phoneNumber", phoneNumbW);
            startActivity(intent);
        }
        /*else{
            GetCreditInfoModel gci = JsonHelper.ToModel(json,GetCreditInfoModel.class);
            gci.Koplate = Float.parseFloat("10");
            Intent intent = new Intent(MainActivity.this, FirstWin.class);
            intent.putExtra(GetCreditInfoModel.class.getSimpleName(),gci);
            startActivity(intent);
        }*/
    }
}

