package com.example.phone;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.example.phone.Classes.Callback;
import com.example.phone.Helpers.BitmapHelper;
import com.example.phone.Helpers.CameraResolutionHelper;
import com.example.phone.Helpers.HttpWeb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class RegistrationFirstWin extends AppCompatActivity implements SurfaceHolder.Callback,
        Camera.PictureCallback, Camera.PreviewCallback, Camera.AutoFocusCallback, Callback {

    Camera camera = null;
    SurfaceHolder surfaceHolder;
    SurfaceView preview;
    Button shotBtn;
    Dialog dialog;
    ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_first_win);

        preview = (SurfaceView) findViewById(R.id.surfaceView);

        dialog = new Dialog(RegistrationFirstWin.this);
        dialog.setContentView(R.layout.photo_dialog);

        surfaceHolder = preview.getHolder();
        surfaceHolder.addCallback(this);

        iv = (ImageView) findViewById(R.id.imageView);

        shotBtn = (Button) findViewById(R.id.shotBtn);

        shotBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getPackageManager();
                if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)){
                    camera.autoFocus(RegistrationFirstWin.this);
                }
                else {
                    try {
                        camera.takePicture(null, null, null, RegistrationFirstWin.this);
                    } catch (Exception e){
                        e.getMessage();
                    }
                }
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        try {
            camera = Camera.open();
        }
        catch (Exception ex){
            ex.getMessage();
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        if (camera != null)
        {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {
        if (success)
        {
            try {
                camera.takePicture(null, null, null, this);
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    @Override
    public void onPictureTaken(byte[] data, final Camera camera) {
        try
        {
            /*File saveDir = new File("/sdcard/AppKredPhoto/");
            if (!saveDir.exists())
            {
                saveDir.mkdirs();
            }
            FileOutputStream os = new FileOutputStream("/sdcard/AppKredPhoto/identification.jpg");
            os.write(data);
            os.close();*/
            Bitmap bm = BitmapFactory.decodeByteArray(data,0,data.length);
            bm = BitmapHelper.RotateBitmap(bm,90);

            SubsamplingScaleImageView ivDial = dialog.findViewById(R.id.imageView3);
            ivDial.setImage(ImageSource.bitmap(bm));
            camera.stopPreview();
            dialog.show();

            Button cancel = dialog.findViewById(R.id.button2);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    camera.startPreview();
                }
            });

            Button ok = dialog.findViewById(R.id.button);
            final Bitmap finalBm = bm;
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HttpWeb http = new HttpWeb(getString(R.string.SendFile),"id",true, RegistrationFirstWin.this);
                    http.setFile(BitmapHelper.BitmapToByteArray(finalBm));
                    http.registerCallBack(RegistrationFirstWin.this);
                    http.execute();

                    Intent intent = new Intent(RegistrationFirstWin.this, RegistrationSecondWin.class);
                    startActivity(intent);
                }
            });
        }
        catch (Exception e)
        {
            e.getMessage();
        }
        //camera.startPreview();
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try
        {
            camera.setPreviewDisplay(holder);
            camera.setPreviewCallback(this);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        // оптимальное разрешение камеры
        Camera.Parameters parameters=camera.getParameters();
        Camera.Size size = CameraResolutionHelper.getHdSize(parameters);
        if(size==null)
            size = CameraResolutionHelper.getMaxSize(parameters);
        if(size.height>=1024)
            size = CameraResolutionHelper.getApproxHdSize(parameters);
        parameters.setPictureSize(size.width,size.height);
        camera.setParameters(parameters);

        //настройка превью
        Camera.Size previewSize = camera.getParameters().getPreviewSize();
        float aspect = (float) previewSize.width / previewSize.height;
        int previewSurfaceWidth = preview.getWidth();
        int previewSurfaceHeight = preview.getHeight();
        ViewGroup.LayoutParams lp = preview.getLayoutParams();
        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE)
        {
            camera.setDisplayOrientation(90);
            lp.height = previewSurfaceHeight;
            lp.width = (int) (previewSurfaceHeight / aspect);
        }
        else
        {
            camera.setDisplayOrientation(0);
            lp.width = previewSurfaceWidth;
            lp.height = (int) (previewSurfaceWidth / aspect);
        }

        preview.setLayoutParams(lp);
        camera.startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void callBackReturn(String json) {

    }
}
