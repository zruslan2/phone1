package com.example.phone;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.phone.Classes.Callback;
import com.example.phone.Classes.Models.ClientData;
import com.example.phone.Classes.Models.GetCreditInfoModel;
import com.example.phone.Helpers.HttpWeb;
import com.example.phone.Helpers.JsonHelper;

public class FirstWin extends AppCompatActivity implements Callback {

    //Boolean hasCredit = true;
    String[] listPartOfTheDay = new String[] {"Доброе утро ", "Добрый день ", "Добрый вечер "};
    GetCreditInfoModel gci = null;
    ClientData cd = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_win);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.container, new BlankFragment());
        ft.commit();

        Intent intent = getIntent();
        cd = (ClientData) intent.getSerializableExtra(ClientData.class.getSimpleName());

        if(cd.HaveCredit==true){
            HttpWeb http = new HttpWeb(getString(R.string.GetCreditInfo),
                    "?ClientID="+cd.ClientID,false,this);
            http.registerCallBack(FirstWin.this);
            http.execute();
        }
        else
            change();
    }

    public void change(){
        Fragment fragment = null;

        if(cd.HaveCredit==false)
            fragment = new DontHaveCreditFragment().newInstance(cd, listPartOfTheDay);
        else
            fragment = new HaveCreditFragment().newInstance(gci, cd, listPartOfTheDay);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    @Override
    public void callBackReturn(String json) {
        if(json!=null){
            gci = JsonHelper.ToModel(json,GetCreditInfoModel.class);

            change();
        }
    }
}
