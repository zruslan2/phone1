package com.example.phone.network;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.example.phone.Helpers.AppSignatureHelper;
import com.example.phone.Helpers.HashMD5;
import com.example.phone.Helpers.HttpWeb;
import com.example.phone.R;

import java.util.ArrayList;

public class HttpAccess {
    private static String Login;
    private HashMD5 hash;
    public static String SettingKey;
    public static String SettingConst;

    public static final String TAG = HttpAccess.class.getSimpleName();

    public HttpAccess(Context context)
    {
        hash = new HashMD5();
        Login = SettingKey;

        if (Login == null || Login.isEmpty())
            GetSecrets(context);
    }

    public static void GetSecrets(Context context)
    {
        AppSignatureHelper appSignatureHelper = new AppSignatureHelper(context);
        ArrayList<String> appSignature = appSignatureHelper.getAppSignatures();
        String appHash = appSignatureHelper.getAppHash();
        Log.v(TAG, appSignature.get(0));
        Log.v(TAG, "AppHash: " + appHash);
        SettingKey = appSignature.get(0);
        SettingConst = context.getString(R.string.AppConstant);
        Login = SettingKey;
    }

    private String GetLogin()
    {
        return hash.md5(Login);
    }

    private String GetPassword()
    {
        String Password = SettingConst + Login;
        return hash.md5(Password);
    }

    public String GetAccess()
    {
        return "Basic " + Base64.encodeToString((GetLogin() + ":" + GetPassword()).getBytes(), Base64.NO_WRAP);
    }
}
