package com.example.phone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import com.example.phone.Classes.Callback;
import com.example.phone.Classes.MemoryType;
import com.example.phone.Classes.Models.ClientData;
import com.example.phone.Classes.Models.GetCreditInfoModel;
import com.example.phone.Helpers.HttpWeb;
import com.example.phone.Helpers.JsonHelper;
import com.example.phone.Helpers.PrefHelper;
import com.example.phone.network.HttpAccess;

public class Welcome extends AppCompatActivity implements Callback {

    String clientId = null;
    String phoneNumber;
    ClientData cd = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        //имеет кредит
/*        PrefHelper.setValue(this,"phoneNumber","7019366996",MemoryType.STRING);
        PrefHelper.setValue(this,"clientId","10029",MemoryType.STRING);*/

        //зарегистрированный но кредита нет
/*        PrefHelper.setValue(this,"phoneNumber","7074039246",MemoryType.STRING);
        PrefHelper.setValue(this,"clientId","331255",MemoryType.STRING);*/

        //новый пользователь
        PrefHelper.setValue(this,"phoneNumber","",MemoryType.STRING);
        PrefHelper.setValue(this,"clientId","",MemoryType.STRING);

        clientId = (String) PrefHelper.getValue(this,"clientId", MemoryType.STRING);
        phoneNumber = (String) PrefHelper.getValue(this,"phoneNumber", MemoryType.STRING);

        HttpAccess.GetSecrets(this);
        if(clientId!=null&&!(clientId.equals("")))
        {
            HttpWeb http = new HttpWeb(getString(R.string.GetClientData),
                    "?ClientID="+clientId+"&PhoneNumber="+phoneNumber,false,this);
            http.registerCallBack(Welcome.this);
            http.execute();
        }
        else{
            Intent intent = new Intent(Welcome.this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void callBackReturn(String json) {
        cd = JsonHelper.ToModel(json,ClientData.class);
        //cd.HaveCredit = true;
        Intent intent = new Intent(Welcome.this, FirstWin.class);
        intent.putExtra(ClientData.class.getSimpleName(),cd);
        startActivity(intent);
    }
}
