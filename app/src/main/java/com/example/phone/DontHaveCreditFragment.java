package com.example.phone;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.phone.Classes.Callback;
import com.example.phone.Classes.Models.ClientData;
import com.example.phone.Classes.Models.GetCreditLimitModel;
import com.example.phone.Helpers.HttpWeb;
import com.example.phone.Helpers.JsonHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DontHaveCreditFragment extends Fragment implements Callback {
    EditText editCreditSum;
    EditText editDaysCount;
    TextView textMessage;
    TextView greeting;
    TextView appeal;
    ProgressBar pb;
    Button receive;
    Integer creditSum;
    Integer countDays;
    Integer maxSum;
    Integer minSum;
    Boolean flag;
    //String clientId;
    //String name, surname;
    String[] listPartOfTheDay;
    ClientData cd;
    GetCreditLimitModel cl;

    public static DontHaveCreditFragment newInstance(ClientData cd, String[] listPartOfTheDay) {
        DontHaveCreditFragment fragment = new DontHaveCreditFragment();
        Bundle bundle = new Bundle();
       /* bundle.putString("name",name);
        bundle.putString("surname",surname);
        bundle.putString("clientId",clientId);*/
        bundle.putSerializable(ClientData.class.getSimpleName(),cd);
        bundle.putStringArray("listPartOfTheDay",listPartOfTheDay);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            /*clientId = getArguments().getString("clientId");
            name = getArguments().getString("name");
            surname = getArguments().getString("surname");*/
            cd = (ClientData) getArguments().getSerializable(ClientData.class.getSimpleName());
            listPartOfTheDay = getArguments().getStringArray("listPartOfTheDay");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dont_have_credit, container, false);
        editCreditSum = (EditText) view.findViewById(R.id.creditSum);
        editDaysCount = (EditText) view.findViewById(R.id.daysCount);
        textMessage = (TextView) view.findViewById(R.id.textMessage);
        greeting = (TextView) view.findViewById(R.id.greetingText1);
        appeal = (TextView) view.findViewById(R.id.textView5);
        pb = (ProgressBar) view.findViewById((R.id.progressBar4));
        receive = (Button) view.findViewById(R.id.btnReceiveCredit);
        return view;
    }

    @Override
    public void onStart () {
        super.onStart();
        flag = false;

        Date dateNow = Calendar.getInstance().getTime();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("HH");
        Integer cHour = Integer.parseInt(formatForDateNow.format(dateNow));
        if(cHour>=6&&cHour<12)
            greeting.setText(listPartOfTheDay[0]+"\n"+cd.Fam+" "+cd.Name+"!");
        else if(cHour>=12&&cHour<18)
            greeting.setText(listPartOfTheDay[1]+"\n"+cd.Fam+" "+cd.Name+"!");
        else
            greeting.setText(listPartOfTheDay[2]+"\n"+cd.Fam+" "+cd.Name+"!");

        HttpWeb http = new HttpWeb(getString(R.string.GetCreditLimit),
                "?ClientID="+cd.ClientID,false, getContext());
        http.registerCallBack(DontHaveCreditFragment.this);
        http.execute();
        pb.setVisibility(View.VISIBLE);

        receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag==false)
                {
                    appeal.setText("Вы можете получить кредит\n в сумме от "+minSum+" до "
                            +maxSum+" тг\nна срок от "+cl.DaysMinCount+" до "+cl.DaysMaxCount+" дней");
                    editCreditSum.setVisibility(View.VISIBLE);
                    editCreditSum.setSelection(editCreditSum.getText().length());
                    editDaysCount.setVisibility(View.VISIBLE);
                    flag=true;
                }
                else
                {
                    if(Integer.parseInt(editDaysCount.getText().toString())<=cl.DaysMaxCount
                            && Integer.parseInt(editDaysCount.getText().toString())>=cl.DaysMinCount
                            && Integer.parseInt(editCreditSum.getText().toString())>=minSum
                            && Integer.parseInt(editCreditSum.getText().toString())<=maxSum)
                    {
                        creditSum = Integer.parseInt(editCreditSum.getText().toString());
                        countDays = Integer.parseInt(editDaysCount.getText().toString());
                        //textMessage.setTextColor(getResources().getColor(R.color.green));
                        //textMessage.setText("Поздравляем Вы получили кредит\nна сумму: "+creditSum);
                        editCreditSum.setVisibility(View.GONE);
                        editDaysCount.setVisibility(View.GONE);
                        flag = false;
                   /* Fragment fragment = null;
                    fragment = new CreateAccountFragment().newInstance(creditSum, phoneNumber);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.container, fragment);
                    ft.commit();*/
                    }
                    else{
                        textMessage.setTextColor(getResources().getColor(R.color.red));
                        textMessage.setText("У Вас не правильно указанны данные");
                    }

                }
            }
        });

        editDaysCount.addTextChangedListener(new TextWatcher() {
            Boolean isBackspaceClicked;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (after < count) {
                    isBackspaceClicked = true;
                } else {
                    isBackspaceClicked = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>0) {
                    if(Integer.parseInt(s.toString())>cl.DaysMaxCount)
                    {
                        textMessage.setTextColor(getResources().getColor(R.color.red));
                        textMessage.setText("Колличество дней не может превышать: " + cl.DaysMaxCount);
                        editCreditSum.setSelection(editCreditSum.getText().length());
                    }
                    else if(Integer.parseInt(s.toString())<cl.DaysMinCount)
                    {
                        textMessage.setTextColor(getResources().getColor(R.color.red));
                        textMessage.setText("Колличество дней не может быть меньше: " + cl.DaysMinCount);
                        editCreditSum.setSelection(editCreditSum.getText().length());
                    }
                    else
                        textMessage.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        editCreditSum.addTextChangedListener(new TextWatcher() {
            Boolean isBackspaceClicked;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (after < count) {
                    isBackspaceClicked = true;
                } else {
                    isBackspaceClicked = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>0) {
                    if(Integer.parseInt(s.toString())>maxSum)
                    {
                        textMessage.setTextColor(getResources().getColor(R.color.red));
                        textMessage.setText("Cумма кредита не может превышать: " + maxSum+" тг");
                        editCreditSum.setSelection(editCreditSum.getText().length());
                    }
                    else if(Integer.parseInt(s.toString())<minSum)
                    {
                        textMessage.setTextColor(getResources().getColor(R.color.red));
                        textMessage.setText("Cумма кредита не может быть меньше: " + minSum+" тг");
                        editCreditSum.setSelection(editCreditSum.getText().length());
                    }
                    else if(Integer.parseInt(s.toString())<maxSum&&Integer.parseInt(s.toString())>minSum&&Integer.parseInt(s.toString())%1000!=0)
                    {
                        textMessage.setTextColor(getResources().getColor(R.color.red));
                        textMessage.setText("Cумма кредита должна быть кратной тысячи");
                        editCreditSum.setSelection(editCreditSum.getText().length());
                    }
                    else
                        textMessage.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }



    @Override
    public void callBackReturn(String json) {
        if(json!=null){
            pb.setVisibility(View.GONE);
            cl = JsonHelper.ToModel(json,GetCreditLimitModel.class);
            if(Integer.parseInt(cl.CreditMaxSum)%1000!=0)
                maxSum = (Integer.parseInt(cl.CreditMaxSum)/1000)*1000;
            else
                maxSum = Integer.parseInt(cl.CreditMaxSum);
            if(cl.CreditMinSum%1000!=0)
                minSum = ((cl.CreditMinSum/1000)+1)*1000;
            else
                minSum = cl.CreditMinSum;
        }
    }
}
