package com.example.phone;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.content.Intent;

import com.example.phone.Classes.Callback;
import com.example.phone.Classes.MemoryType;
import com.example.phone.Classes.Models.CheckPhoneModel;
import com.example.phone.Helpers.HttpWeb;
import com.example.phone.Helpers.JsonHelper;
import com.example.phone.Helpers.PrefHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class GetCode extends AppCompatActivity implements Callback {
    //String clientId;

    private final int MY_PERMISSIONS_REQUEST_CAMERA = 100;

    Boolean HasCredit = true;
    String phoneNumber;
    TextView error;
    ProgressBar progress;
    EditText code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_code);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        //clientId = intent.getStringExtra("clientId");
        phoneNumber = intent.getStringExtra("phoneNumber");

        error = (TextView) findViewById(R.id.textError);
        progress = (ProgressBar) findViewById(R.id.progressBar2);

        code = (EditText) findViewById(R.id.editText2);
        code.addTextChangedListener(new TextWatcher() {
            Boolean isBackspaceClicked;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (after < count) {
                    isBackspaceClicked = true;
                } else {
                    isBackspaceClicked = false;
                }
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(isBackspaceClicked){
                    error.setText("");
                }
                if(s.length()==4) {
                        CheckPhoneModel cpm = new CheckPhoneModel(phoneNumber,s.toString());
                        String json = JsonHelper.ToJson(cpm);
                        HttpWeb http = new HttpWeb(getString(R.string.CheckPhoneConfirmationCode),json,true, GetCode.this);
                        http.registerCallBack(GetCode.this);
                        http.execute();
                        code.setEnabled(false);
                        progress.setVisibility(View.VISIBLE);
                    }
                   /* else
                    {
                        error.setTextColor(getResources().getColor(R.color.red));
                        error.setText("Вы ввели не правильно код");
                        error.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                code.setText("");
                                error.setText("");
                            }
                        }, 1500);
                    }*/
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()>4){
                    code.setText(s.toString().substring(0,s.length()-1));
                    code.setSelection(code.getText().length());
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(GetCode.this, Registration.class);
                    startActivity(intent);
                } else {

                }
                return;
            }
        }
    }

    @Override
    public void callBackReturn(String json) {
        /*if(Integer.parseInt(json)<0){
            error.setTextColor(getResources().getColor(R.color.red));
            error.setText("Вы ввели не правильно код");
            progress.setVisibility(View.GONE);
            code.setEnabled(true);
        }
        else if(Integer.parseInt(json)==0){ //не зарегистрированный пользователь*/
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(GetCode.this, Registration.class);
            startActivity(intent);
        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        }
            //Intent intent = new Intent(GetCode.this, RegistrationFirstWin.class);
            //intent.putExtra("clientId", clientId);
            //intent.putExtra("phoneNumber", phoneNumber);
            //startActivity(intent);
       /* }
        else {
            //PrefHelper.setValue(this,"clientId", json, MemoryType.STRING);
            *//*PrefHelper.setValue(this,"phoneNumber", phoneNumber, MemoryType.STRING);
            Intent intent = new Intent(GetCode.this, RegistrationFirstWin.class);
            //intent.putExtra("clientId", clientId);
            intent.putExtra("phoneNumber", phoneNumber);
            startActivity(intent);*//*
        }*/
    }
}
