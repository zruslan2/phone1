package com.example.phone;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.example.phone.Classes.Callback;
import com.example.phone.Helpers.BitmapHelper;
import com.example.phone.Helpers.CameraResolutionHelper;
import com.example.phone.Helpers.HttpWeb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class RegistrationSecondWin extends AppCompatActivity implements SurfaceHolder.Callback,
        Camera.PictureCallback, Camera.PreviewCallback, Camera.AutoFocusCallback, Callback {

    Camera camera = null;
    SurfaceHolder surfaceHolder;
    SurfaceView preview;
    Button shotBtn;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_second_win);

        preview = (SurfaceView) findViewById(R.id.surfaceView1);

        dialog = new Dialog(RegistrationSecondWin.this);
        dialog.setContentView(R.layout.photo_dialog);

        surfaceHolder = preview.getHolder();
        surfaceHolder.addCallback(this);

        shotBtn = (Button) findViewById(R.id.shotBtn1);

        shotBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getPackageManager();
                if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)){
                    camera.autoFocus(RegistrationSecondWin.this);
                }
                else {
                    try {
                        camera.takePicture(null, null, null, RegistrationSecondWin.this);
                    } catch (Exception e){
                        e.getMessage();
                    }
                }
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                Camera.CameraInfo info=new Camera.CameraInfo();

                for (int i=0; i < Camera.getNumberOfCameras(); i++) {
                    Camera.getCameraInfo(i, info);

                    if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                        camera=Camera.open(i);
                    }
                }
            }
        }
        catch (Exception ex){
            ex.getMessage();
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        if (camera != null)
        {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {
        if (success)
        {
            try {
                camera.takePicture(null, null, null, this);
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    @Override
    public void onPictureTaken(byte[] data, final Camera camera) {
        try
        {
            /*File saveDir = new File("/sdcard/AppKredPhoto/");
            if (!saveDir.exists())
            {
                saveDir.mkdirs();
            }
            FileOutputStream os = new FileOutputStream("/sdcard/AppKredPhoto/face.jpg");
            os.write(data);
            os.close();*/
            Bitmap bm = BitmapFactory.decodeByteArray(data,0,data.length);
            bm = BitmapHelper.RotateBitmap(bm,270);

            SubsamplingScaleImageView ivDial = dialog.findViewById(R.id.imageView3);
            ivDial.setImage(ImageSource.bitmap(bm));
            camera.stopPreview();
            dialog.show();

            Button cancel = dialog.findViewById(R.id.button2);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    camera.startPreview();
                }
            });

            Button ok = dialog.findViewById(R.id.button);
            final Bitmap finalBm = bm;
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HttpWeb http = new HttpWeb(getString(R.string.SendFace),"face",true, RegistrationSecondWin.this);
                    http.setFile(BitmapHelper.BitmapToByteArray(finalBm));
                    http.registerCallBack(RegistrationSecondWin.this);
                    http.execute();
                }
            });
        }
        catch (Exception e)
        {
            e.getMessage();
        }
        camera.startPreview();
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try
        {
            camera.setPreviewDisplay(holder);
            camera.setPreviewCallback(this);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        // оптимальное разрешение камеры
        Camera.Parameters parameters=camera.getParameters();
        Camera.Size size = CameraResolutionHelper.getHdSize(parameters);
        if(size==null)
            size = CameraResolutionHelper.getMaxSize(parameters);
        if(size.height>=1024)
            size = CameraResolutionHelper.getApproxHdSize(parameters);
        parameters.setPictureSize(size.width,size.height);
        camera.setParameters(parameters);

        //настройка превью
        Camera.Size previewSize = camera.getParameters().getPreviewSize();
        float aspect = (float) previewSize.width / previewSize.height;
        int previewSurfaceWidth = preview.getWidth();
        int previewSurfaceHeight = preview.getHeight();
        ViewGroup.LayoutParams lp = preview.getLayoutParams();
        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE)
        {
            camera.setDisplayOrientation(90);
            lp.height = previewSurfaceHeight;
            lp.width = (int) (previewSurfaceHeight / aspect);
        }
        else
        {
            camera.setDisplayOrientation(0);
            lp.width = previewSurfaceWidth;
            lp.height = (int) (previewSurfaceWidth / aspect);
        }

        preview.setLayoutParams(lp);
        camera.startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void callBackReturn(String json) {

    }
}
