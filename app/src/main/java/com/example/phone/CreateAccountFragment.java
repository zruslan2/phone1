package com.example.phone;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.phone.Helpers.IsInteger;

public class CreateAccountFragment extends Fragment {
    EditText phone;
    EditText name;
    EditText surname;
    EditText idNumber;
    TextView mes;
    Button btnRec;
    Double creditSum;
    String recPhoneNumber;


    public static CreateAccountFragment newInstance(Double creditSum, String phoneNumber) {
        CreateAccountFragment fragment = new CreateAccountFragment();
        Bundle args = new Bundle();
        args.putDouble("creditSum", creditSum);
        args.putString("phoneNumber",phoneNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            creditSum = getArguments().getDouble("creditSum");
            recPhoneNumber = getArguments().getString("phoneNumber");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_account, container, false);
        phone = (EditText) view.findViewById(R.id.phone);
        name = (EditText) view.findViewById(R.id.name);
        surname = (EditText) view.findViewById(R.id.surname);
        idNumber = (EditText) view.findViewById(R.id.idNumber);
        btnRec = (Button) view.findViewById(R.id.btnReceive);
        mes = (TextView) view.findViewById(R.id.textMessage);
        return view;
    }
    @Override
    public void onStart () {
        super.onStart();

        phone.setText(recPhoneNumber);
        phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        phone.addTextChangedListener(new TextWatcher() {
            Boolean isBackspaceClicked;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (after < count) {
                    isBackspaceClicked = true;
                } else {
                    isBackspaceClicked = false;
                }
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(isBackspaceClicked && s.length()==2)
                {
                    phone.setText("+77");
                    phone.setSelection(phone.getText().length());
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
                if(!IsInteger.IsaInteger(s.toString().substring(s.length()-1)))
                {
                    phone.setText(s.toString().substring(0,s.length()-1));
                    phone.setSelection(phone.getText().length());
                }
            }
        });

        name.setFilters(new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence source, int start,
                                               int end, Spanned dest, int dstart, int dend) {
                        if(source.equals("")){
                            return source;
                        }
                        if(source.toString().matches("[a-zA-Zа-яА-Я]+")){
                            return source;
                        }
                        return "";
                    }
                }
        });

        surname.setFilters(new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                        if(source.equals("")){
                            return source;
                        }
                        if(source.toString().matches("[a-zA-Zа-яА-Я]+")){
                            return source;
                        }
                        return "";
                    }
                }
        });
        btnRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().toString().equals("")||surname.getText().toString().equals("")||
                phone.getText().toString().equals("")||idNumber.getText().toString().equals("")){
                    mes.setTextColor(getResources().getColor(R.color.red));
                    mes.setText("Все поля должны быть заполненны");
                }
                else if(idNumber.getText().toString().length()!=9)
                {
                    mes.setTextColor(getResources().getColor(R.color.red));
                    mes.setText("Вы неправильно указали номер удостоверения");
                }
                else
                {
                    mes.setTextColor(getResources().getColor(R.color.green));
                    mes.setText("Поздравляем "+name.getText().toString()+", Вы получили кредит\nна сумму "+creditSum+" тг");
                }
            }
        });
    }
}
