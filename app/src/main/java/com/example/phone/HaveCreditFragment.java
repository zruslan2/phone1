package com.example.phone;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.phone.Classes.Models.ClientData;
import com.example.phone.Classes.Models.GetCreditInfoModel;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static java.lang.Math.abs;


public class HaveCreditFragment extends Fragment {

    TextView greeting;
    TextView creditSum;
    TextView endTime;
    TextView dogNum;
    TextView endDays;
    TextView koplate;
    Integer absCh;
    String[] listPartOfTheDay;
    Double minSum;
    String temp;
    GetCreditInfoModel gci;
    ClientData cd;
    String[] Days = new String[] {" день", " дня", " дней"};
    Button dog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null) {
            listPartOfTheDay = getArguments().getStringArray("listPartOfTheDay");
            gci = (GetCreditInfoModel)getArguments().getSerializable(GetCreditInfoModel.class.getSimpleName());
            cd = (ClientData) getArguments().getSerializable(ClientData.class.getSimpleName());
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_have_credit, container, false);
        greeting = (TextView)view.findViewById(R.id.greetingText);
        creditSum = (TextView)view.findViewById(R.id.CreditSum);
        endTime = (TextView)view.findViewById(R.id.EndTime);
        dogNum = (TextView)view.findViewById(R.id.DogNum);
        endDays = (TextView)view.findViewById(R.id.EndDays);
        koplate = (TextView)view.findViewById(R.id.Koplate);
        dog = (Button)view.findViewById(R.id.btnDog);
        return view;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onStart ()  {
        super .onStart ();
        minSum = 1000.0;
        //flag = false;
        Date dateNow = Calendar.getInstance().getTime();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("HH");
        Integer cHour = Integer.parseInt(formatForDateNow.format(dateNow));
        if(cHour>=6&&cHour<12)
            greeting.setText(listPartOfTheDay[0]+"\n"+cd.Fam+" "+cd.Name+"!\nУ Вас имеется кредит:");
        else if(cHour>=12&&cHour<18)
            greeting.setText(listPartOfTheDay[1]+"\n"+cd.Fam+" "+cd.Name+"!\nУ Вас имеется кредит:");
        else
            greeting.setText(listPartOfTheDay[2]+"\n"+cd.Fam+" "+cd.Name+"!\nУ Вас имеется кредит:");

        creditSum.setText(gci.CreditSum.toString()+" тенге");
        endTime.setText(gci.EndTime);
        dogNum.setText(gci.DogNum);
        if(gci.EndDays<0) {
            absCh = abs(gci.EndDays);
            endDays.setTextColor(getResources().getColor(R.color.red));
            temp = gci.EndDays.toString().substring(gci.EndDays.toString().length()-1);
            if(Integer.parseInt(temp)==1)
                endDays.setText(absCh.toString()+Days[0]);
            else if(Integer.parseInt(temp)>1&&Integer.parseInt(temp)<5)
                endDays.setText(absCh.toString()+Days[1]);
            else if(Integer.parseInt(temp)>4||Integer.parseInt(temp)==0)
                endDays.setText(absCh.toString()+Days[2]);
        }
        else
            endDays.setText("нет");
        koplate.setText(gci.Koplate.toString()+" тенге");
        dog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ContractActivity.class);
                intent.putExtra(ClientData.class.getSimpleName(),cd);
                intent.putExtra(GetCreditInfoModel.class.getSimpleName(),gci);
                startActivity(intent);
            }
        });
    }

    public static HaveCreditFragment newInstance(GetCreditInfoModel gci,ClientData cd, String[] listPartOfTheDay/*String name, String surname, String[] listPartOfTheDay, Double creditSum*/) {
        HaveCreditFragment f = new HaveCreditFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ClientData.class.getSimpleName(),cd);
        bundle.putSerializable(GetCreditInfoModel.class.getSimpleName(), gci);
        bundle.putStringArray("listPartOfTheDay", listPartOfTheDay);
        f.setArguments(bundle);
        return f;
    }
}
