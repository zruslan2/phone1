package com.example.phone.Classes.Models;

import java.io.Serializable;

public class ClientData implements Serializable {
    public String ClientID, Phone, email, Name, Fam, Och, Country, AdditionalClientData;
    public Boolean HaveSelfie, HaveDocPart1, HaveDocPart2, PhoneIsConfirmed, EmailIsConfirmed, HaveCredit, HavePayRequest;
}
