package com.example.phone.Classes.Models;

public class DocumentModel {
    public String birthday;
    public String doc_type;
    public String expire_date;
    public String firstname;
    public String surname;
    public String gender;
    public String country;
    public String doc_num;
    public String INN;
}
