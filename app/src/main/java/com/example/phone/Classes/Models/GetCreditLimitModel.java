package com.example.phone.Classes.Models;

import java.io.Serializable;

public class GetCreditLimitModel implements Serializable {
    public String Name, CreditMaxSum;
    public Integer CreditMinSum, DaysMinCount, DaysMaxCount;
}
