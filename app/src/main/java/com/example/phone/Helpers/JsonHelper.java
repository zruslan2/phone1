package com.example.phone.Helpers;


import com.example.phone.Classes.Models.IModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonHelper {

    public static String ToJson(IModel model){
        Gson json = new Gson();
        return json.toJson(model);
    }
    public static<T> T ToModel(String sjson, Class<T> cls){
        GsonBuilder builder = new GsonBuilder();
        Gson json = builder.create();
        return json.fromJson(sjson, cls);
    }
}
