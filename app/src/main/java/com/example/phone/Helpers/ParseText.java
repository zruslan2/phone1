package com.example.phone.Helpers;

import android.text.TextUtils;

import com.example.phone.Classes.Models.DocumentModel;

public class ParseText {

    private static boolean CheckText(String text){
        text = text.replace(" ","");
        Integer ind = text.indexOf("IDKAZ");
        if(ind==-1)
            return false;
        else {
            String MRZ = GetMRZ(text);
            if(IdType(MRZ)==1){
                if(MRZ.length()==90)
                    return true;
                else
                    return false;
            }
            else{
                if(MRZ.length()==72)
                    return true;
                else
                    return false;
            }
            /*return true;*/
        }
    }

    private static Integer IdType(String MRZ){
        if(MRZ.length()>=85 && MRZ.length()<=95)
            return 1; //новое удостоверение (MRZ 3 строки)
        else
            return 2; //удостоверение старого образца (MRZ 2 строки)
    }

    private static String GetMRZ(String text){
        String txt = text.replace(" ","");
        String MRZ = txt.substring(txt.indexOf("IDKAZ"),txt.length()).replace("\n","");
        return FixMRZ(MRZ);
    }

    private static String FixMRZ(String MRZ){
        String NewMRZ = null;
        Integer tmp = 0;
        String firstStr = null;
        String secondStr = null;
        String thridStr = null;
        if(IdType(MRZ)==1){
                firstStr = MRZ.substring(0,GetIndexSecondStr(MRZ));
                secondStr = MRZ.substring(GetIndexSecondStr(MRZ),GetIndexThridStr(MRZ));
                if(MRZ.length()-GetIndexThridStr(MRZ)<=30)
                    thridStr = MRZ.substring(GetIndexThridStr(MRZ));
                else
                    thridStr = MRZ.substring(GetIndexThridStr(MRZ),GetIndexThridStr(MRZ)+30);

                firstStr = firstStr.replace('O','0');
                if(firstStr.length()<30)
                {
                    tmp = 30 - firstStr.length();
                    for(int i = 0;i < tmp;i++)
                        firstStr = firstStr+"<";
                    tmp=0;
                }

                secondStr = secondStr.replace('O','0');
                if(secondStr.length()<30)
                {
                    tmp = 30 - secondStr.length();
                    secondStr = InsertIntoSecondStr(secondStr,tmp);
                    tmp = 0;
                }
                thridStr = CheckAndFixDelimeterNameAndSurname(thridStr,1);
                if(thridStr.length()<30)
                {
                    tmp = 30 - thridStr.length();
                    for(int i = 0;i < tmp;i++)
                        thridStr = thridStr+"<";
                    tmp = 0;
                }
                NewMRZ = firstStr + secondStr + thridStr;
        }
        else{
            firstStr = MRZ.substring(0,GetIndexSecondStr(MRZ));
            if(MRZ.length()-GetIndexSecondStr(MRZ)<=36)
                secondStr = MRZ.substring(GetIndexSecondStr(MRZ));
            else
                secondStr = MRZ.substring(GetIndexSecondStr(MRZ),GetIndexSecondStr(MRZ)+36);

            firstStr = firstStr.toUpperCase();
            firstStr = CheckAndFixDelimeterNameAndSurname(firstStr,2);
            if(firstStr.length()<36){
                tmp = 36 - firstStr.length();
                for(int i = 0;i < tmp;i++)
                    firstStr = firstStr+"<";
                tmp=0;
            }
            if(firstStr.length()>36)
                firstStr = firstStr.substring(0,36);

            secondStr = secondStr.toUpperCase();
            secondStr = secondStr.replace('O','0');
            if(secondStr.length()<36){
                tmp = 36 - secondStr.length();
                secondStr = InsertIntoSecondStr(secondStr,tmp);
                tmp = 0;
            }
            NewMRZ = firstStr + secondStr;
        }
        return NewMRZ;
    }

    private static String CheckAndFixDelimeterNameAndSurname(String str, Integer typeID){
        Integer firstCharDel = str.indexOf("<");
        if(str.charAt(firstCharDel+1)=='<')
            return str;
        else{
            if(typeID==1){
                if(str.charAt(firstCharDel-1)=='K'||str.charAt(firstCharDel-1)=='S')
                {
                    char[] tmpchar = str.toCharArray();
                    tmpchar[firstCharDel-1] = '<';
                    str = String.valueOf(tmpchar);
                }
                else if(str.charAt(firstCharDel+1)=='K'||str.charAt(firstCharDel+1)=='S'&&str.length()==30)
                {
                    char[] tmpchar = str.toCharArray();
                    tmpchar[firstCharDel+1] = '<';
                    str = String.valueOf(tmpchar);
                }
                else{
                    String before = str.substring(0,firstCharDel);
                    String after = str.substring(firstCharDel);
                    before = before + "<";
                    str = before + after;
                    if(str.length()>30)
                        str = str.substring(0,30);
                }
            }
            else{
                if(str.charAt(firstCharDel-1)=='K'||str.charAt(firstCharDel-1)=='S')
                {
                    char[] tmpchar = str.toCharArray();
                    tmpchar[firstCharDel-1] = '<';
                    str = String.valueOf(tmpchar);
                }
                else if(str.charAt(firstCharDel+1)=='K'||str.charAt(firstCharDel+1)=='S'&&str.length()==36)
                {
                    char[] tmpchar = str.toCharArray();
                    tmpchar[firstCharDel+1] = '<';
                    str = String.valueOf(tmpchar);
                }
                else{
                    String before = str.substring(0,firstCharDel);
                    String after = str.substring(firstCharDel);
                    before = before + "<";
                    str = before + after;
                    if(str.length()>36)
                        str = str.substring(0,36);
                }
            }
            return str;
        }
    }

    private static String InsertIntoSecondStr(String Str, Integer count){
        String before = Str.substring(0,Str.length()-1);
        String after = Str.substring(Str.length()-1);
        for(int i = 0;i<count;i++)
            before = before + "<";
        return before + after;
    }

    private static Integer GetIndexSecondStr(String MRZ){
        if(IdType(MRZ)==1){
            for(int i=28;i<31;i++){
                if(Character.isDigit(MRZ.charAt(i)))
                    return i;
            }
        }
        else {
            for(int i=34;i<37;i++){
                if(Character.isDigit(MRZ.charAt(i)))
                    return i;
            }
        }
        return 0;
    }
    private static Integer GetIndexThridStr(String MRZ){
        for(int i=57;i<61;i++){
            if(Character.isLetter(MRZ.charAt(i)))
                return i;
        }
        return 0;
    }

    private static String GetIIN(String MRZ){
        String IIN = null;
        if(IdType(MRZ)==1){
            IIN = MRZ.substring(15,27);
        }
        else if(IdType(MRZ)==2){
            IIN = MRZ.substring(49,55)+MRZ.substring(64,70);
        }
        if(CheckIIN(IIN)){
            return IIN;
        }else{
            return null;
        }
    }

    private static Boolean CheckIIN(String IIN){
        Integer[] weight = {1,2,3,4,5,6,7,8,9,10,11};//вес разряда
        Integer[] weight1 = {3,4,5,6,7,8,9,10,11,1,2};// альтернативный вес разряда, если первый раз КР=10
        Integer sum=0;//сумма всех множителей разряд на вес
        Integer incomCD = Integer.parseInt(IIN.substring(11));//входящий контрольный разряд
        Integer CD=0;//контрольный разряд
        for(int i=0;i<11;i++){
            sum+=Integer.parseInt(IIN.substring(i,i+1))*weight[i];
        }
        CD = sum%11;
        if(CD==10){
            sum=0;
            for(int i=0;i<11;i++){
                sum+=Integer.parseInt(IIN.substring(i,i+1))*weight1[i];
            }
            CD=sum%11;
            if(CD==10) return false;
            else {
                if(CD==incomCD) return true;
                else return false;
            }
        }
        else{
            if(CD==incomCD) return true;
            else return false;
        }
    }

    private static String GetID(String MRZ){
        String ID = null;
        if(IdType(MRZ)==1){
            ID = MRZ.substring(5,15);
        }
        else{
            ID = MRZ.substring(36,46);
        }
        if(CheckID(ID))
            return ID.substring(0,ID.length()-1);
        else
            return null;
    }

    private static Boolean CheckID(String ID){
        Integer[] weight = {7,3,1,7,3,1,7,3,1};//вес разряда
        Integer sum=0;//сумма всех множителей разряд на вес
        Integer incomCD = Integer.parseInt(ID.substring(9));//входящий контрольный разряд
        Integer CD=0;//контрольный разряд
        for(int i=0;i<9;i++){
            sum+=Integer.parseInt(ID.substring(i,i+1))*weight[i];
        }
        CD = sum%10;
        if(CD==incomCD)
            return true;
        else
            return false;
    }

    private static String GetExpDate(String MRZ){
        String ExpDate = null;
        if(IdType(MRZ)==1){
            ExpDate = MRZ.substring(38,45);
        }
        else{
            ExpDate = MRZ.substring(57,64);
        }
        if(CheckExpDate(ExpDate)){
            return String.format("%s.%s.20%s",ExpDate.substring(4,6),ExpDate.substring(2,4),ExpDate.substring(0,2));
        }
        else
            return null;
    }

    private static Boolean CheckExpDate(String ExpDate){
        Integer[] weight = {7,3,1,7,3,1};//вес разряда
        Integer sum=0;//сумма всех множителей разряд на вес
        Integer incomCD = Integer.parseInt(ExpDate.substring(6));//входящий контрольный разряд
        Integer CD=0;//контрольный разряд
        for(int i=0;i<6;i++){
            sum+=Integer.parseInt(ExpDate.substring(i,i+1))*weight[i];
        }
        CD = sum%10;
        if(CD==incomCD)
            return true;
        else
            return false;
    }

    private static String GetBirthDay(String IIN){
        String cen = null;
        if(IIN.substring(6,7).equals("1")||IIN.substring(6,7).equals("2"))
            cen = "18";
        else if(IIN.substring(6,7).equals("3")||IIN.substring(6,7).equals("4"))
            cen = "19";
        else if(IIN.substring(6,7).equals("5")||IIN.substring(6,7).equals("6"))
            cen = "20";
        return String.format("%s.%s.%s",IIN.substring(4,6),IIN.substring(2,4),cen+IIN.substring(0,2));
    }

    private static String GetGender(String IIN){
        String gender = null;
        if(IIN.substring(6,7).equals("1")||IIN.substring(6,7).equals("3")||IIN.substring(6,7).equals("5"))
            gender = "M";
        else
            gender = "F";
        return gender;
    }

    private static String GetSurname(String MRZ){
        String surname = null;
        if(IdType(MRZ)==1){
            surname = MRZ.substring(60, MRZ.indexOf("<",60));
        }
        else{
            surname = MRZ.substring(5,MRZ.indexOf("<",5));
        }
        return surname;
    }

    private static String GetName(String MRZ){
        String name = null;
        if(IdType(MRZ)==1){
            name = MRZ.substring(MRZ.indexOf("<",60)+2, MRZ.indexOf("<",MRZ.indexOf("<",60)+2));
        }
        else{
            name = MRZ.substring(MRZ.indexOf("<",5)+2,MRZ.indexOf("<",MRZ.indexOf("<",5)+2));
        }
        return name;
    }

    public static DocumentModel GetDocumentModel(String text){
        DocumentModel dm = new DocumentModel();
        String MRZ = null;
        if(CheckText(text)){
            MRZ = GetMRZ(text);
            dm.country = "KAZ";
            dm.doc_type = "ID";
            dm.INN = GetIIN(MRZ);
            dm.birthday = GetBirthDay(dm.INN);
            dm.doc_num = GetID(MRZ);
            dm.expire_date = GetExpDate(MRZ);
            dm.gender = GetGender(dm.INN);
            dm.firstname = GetName(MRZ);
            dm.surname = GetSurname(MRZ);
        }
        if(dm.INN!=null&&dm.doc_num!=null&&dm.expire_date!=null)
            return dm;
        else
            return null;
    }
}
