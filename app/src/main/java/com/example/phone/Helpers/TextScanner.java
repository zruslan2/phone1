package com.example.phone.Helpers;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Line;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TextScanner {

    private static TextScanner sTextScanner;
    private Context mContext;
    private TextRecognizer mTextRecognizer;
    private Bitmap mBitmap;


    public TextScanner(Context context) {
        this.mContext = context;
    }

    public TextScanner init() {
        mTextRecognizer = new TextRecognizer.Builder(mContext).build();
        return sTextScanner;
    }

    public String load(Bitmap bitmap) {
        mBitmap = bitmap;
        //read(mBitmap);
        return read(mBitmap);
    }

    private String read(Bitmap bitmap) {
        if (isInitialized()) {
            Frame imageFrame = new Frame.Builder()
                    .setBitmap(bitmap)
                    .build();

            final SparseArray<TextBlock> textBlocks = mTextRecognizer.detect(imageFrame);

            return sortTextBlock(textBlocks);
        }
        return null;
    }

    private String sortTextBlock(SparseArray<TextBlock> textBlocks) {
        List<TextBlock> myTextBlock = new ArrayList<>();
        for (int i = 0; i < textBlocks.size(); i++) {
            myTextBlock.add(textBlocks.valueAt(i));
        }

        Collections.sort(myTextBlock, new Comparator<TextBlock>() {
            @Override
            public int compare(TextBlock textBlock1, TextBlock textBlock2) {
                return textBlock1.getBoundingBox().top - textBlock2.getBoundingBox().top;
            }
        });

        return parseText(myTextBlock);
    }

    private String parseText(List<TextBlock> myTextBlock) {
        //List<String> textList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < myTextBlock.size(); i++) {
            TextBlock textBlock = myTextBlock.get(i);
            List<Line> lines = (List<Line>) textBlock.getComponents();

            for (Line line : lines) {
                sb.append(line.getValue()).append("\n");
                //textList.add(line.getValue());
            }
        }
        return sb.toString();
    }


    private boolean isInitialized() {
        if (!mTextRecognizer.isOperational()) {

            Log.d("ScannerTest", "Detector dependencies are not yet available.");

            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = mContext.registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(mContext, "Low Storage", Toast.LENGTH_LONG).show();
            }
        }

        return mTextRecognizer.isOperational();
    }
}
