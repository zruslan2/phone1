package com.example.phone.Helpers;

import android.hardware.Camera;

public class CameraResolutionHelper {

    public static Camera.Size getMaxSize(Camera.Parameters param){
        Camera.Size result = null;
        Integer height=0;
        Integer width=0;
        for(Camera.Size size : param.getSupportedPictureSizes()){
            Integer Area = height*width;
            Integer newArea = size.height*size.width;
            if(newArea>Area){
                result = size;
                height = size.height;
                width = size.width;
            }
        }
        return result;
    }

    public static Camera.Size getHdSize(Camera.Parameters param){
        Camera.Size result = null;
        for(Camera.Size size : param.getSupportedPictureSizes()){
            if(size.height == 720 && size.width == 1280)
                result = size;
        }
        return result;
    }

    public static Camera.Size getApproxHdSize(Camera.Parameters param){
        Camera.Size result = null;
        for(Camera.Size size : param.getSupportedPictureSizes()){
            if(size.height>500 && size.height<=1024 && size.width>1000 && size.width<=1536)
                result = size;
        }
        return result;
    }
}
