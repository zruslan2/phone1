package com.example.phone.Helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.example.phone.Classes.MemoryType;

public class PrefHelper {

    public static Object getValue(Context context, String key, MemoryType mt) {
        SharedPreferences pref = context.getSharedPreferences("pref", context.MODE_PRIVATE);
        Object res;
        switch (mt) {
            case STRING:
                res = pref.getString(key, "");
            break;
            case FLOAT:
                res =  pref.getFloat(key, 0);
            break;
            case INTEGER:
                res =  pref.getInt(key, 0);
            break;
            case BOOLEAN:
                res =  pref.getBoolean(key, true);
            break;
            default:
                res=null;
                break;
        }
        return res;
    }

    public static Boolean setValue(Context context, String key, Object value, MemoryType mt){
        SharedPreferences pref = context.getSharedPreferences("pref", context.MODE_PRIVATE);
        Editor edit = pref.edit();
        switch (mt) {
            case STRING:
                edit.putString(key,value.toString());
                break;
            case FLOAT:
                edit.putFloat(key,Float.parseFloat(value.toString()));
                break;
            case INTEGER:
                edit.putInt(key,Integer.parseInt(value.toString()));
                break;
            case BOOLEAN:
                edit.putBoolean(key,Boolean.parseBoolean(value.toString()));
                break;
            default:
                break;
        }
        return edit.commit();
    }
}
