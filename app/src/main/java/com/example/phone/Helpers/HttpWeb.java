package com.example.phone.Helpers;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.phone.Classes.Callback;
import com.example.phone.network.HttpAccess;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

//import javax.naming.Context;
import javax.net.ssl.HttpsURLConnection;

public class HttpWeb extends AsyncTask<Void,Void,String> {

    private Boolean IsPost = null;
    //private Boolean IsHttps = false;
    private String json = null;
    private String url = null;

    private String name = null;
    private String fileName = null;
    private byte[] file = null;
    private String crlf = "\r\n";
    private String twoHyphens = "--";
    private String boundary =  "*****";

    Callback myCallback;
    private static final int CONNECTION_TIMEOUT = 5000;
    Context context;

    HttpURLConnection con;

    public void registerCallBack(Callback callback){
        this.myCallback = callback;
    }

    public void setFile(byte[] b){
        this.file = b;
    }

    public HttpWeb(String url, String paramsOrJson, Boolean isPost, Context context)
    {
        this.context = context;
        IsPost = isPost;
        if(paramsOrJson.equals("id")){
            name = paramsOrJson;
            fileName = paramsOrJson + ".jpg";
            this.url = url;
        }
        else if(paramsOrJson.equals("face")){
            name = paramsOrJson;
            fileName = paramsOrJson + ".jpg";
            this.url = url;
        }
        else{
            if(isPost==false){
                this.url = url + paramsOrJson;
            }
            else{
                this.url = url;
                this.json = paramsOrJson;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(Void... voids) {
        try {
            return Request();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    protected void onPostExecute(String json) {
        if(!(json.equals(""))) {
            if(myCallback!=null)
                myCallback.callBackReturn(json);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private String Request() throws IOException {
        final URL Url = new URL(this.url);

        if (Url.getProtocol().toLowerCase().equals("https"))
        {
            HttpsSecure.trustAllHosts();
            HttpsURLConnection https = (HttpsURLConnection) Url.openConnection();
            https.setHostnameVerifier(HttpsSecure.DO_NOT_VERIFY);
            con = https;
            https.disconnect();
        } else {
            con = (HttpURLConnection) Url.openConnection();
        }

        if(IsPost==false) {
            con.setRequestMethod("GET");
            HttpAccess httpAccess = new HttpAccess(context);
            con.setRequestProperty("Authorization", httpAccess.GetAccess());
        }
        else if(IsPost==true&&name==null){
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setConnectTimeout(CONNECTION_TIMEOUT);
            con.setReadTimeout(CONNECTION_TIMEOUT);
            final DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.writeBytes(this.json);
            out.flush();
            out.close();
        }
        else{
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setRequestMethod("POST");
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setRequestProperty("Cache-Control", "no-cache");
            con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + this.boundary);

            final DataOutputStream out = new DataOutputStream(con.getOutputStream());

            out.writeBytes(this.twoHyphens + this.boundary + this.crlf);
            out.writeBytes("Content-Disposition: form-data; name=\"" +
                        this.name + "\";filename=\"" +
                        this.fileName + "\"" + this.crlf);
            out.writeBytes(this.crlf);

            out.write(file);

            out.writeBytes(this.crlf);
            out.writeBytes(this.twoHyphens + this.boundary + this.twoHyphens + this.crlf);
            out.flush();
            out.close();
        }

            try (final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String inputLine;
                final StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                return content.toString();
            } catch (final Exception ex) {
                ex.printStackTrace();
                return "";
            } finally {
                con.disconnect();
            }
    }
}
