package com.example.phone.Helpers;

import java.math.BigInteger;
import java.security.MessageDigest;

public class HashMD5 {
    public String md5(String s)
    {
        try
        {
            MessageDigest m = java.security.MessageDigest.getInstance("MD5");
            byte[] digest = m.digest(s.getBytes());
            String hash = new BigInteger(1, digest).toString(16);
            return hash;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return "";
    }
}
